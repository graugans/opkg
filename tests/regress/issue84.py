#!/usr/bin/python3
#
# Reporter: Graham Gower
#
# As ``a1'' is already installed, it is expected that opkg should choose to
# install ``a'' as satisfying the virtual dependency for ``v''. This is not the
# case.
#
#
# Status
# ======
#
# Open.

import opk, cfg, opkgcl

def cleanup():
	opkgcl.remove("a1")
	opkgcl.remove("b1")
	opkgcl.remove("a")
	opkgcl.remove("b")
	opkgcl.remove('c')

opk.regress_init()

o = opk.OpkGroup()
o.add(Package="a", Provides="v", Depends="a1")
o.add(Package="b", Provides="v", Depends="b1")
o.add(Package="c", Depends="v")
o.add(Package="a1")
o.add(Package="b1")

o.write_opk()
o.write_list()

opkgcl.update()

# install ``a1`` directly
opkgcl.install("a1_1.0_all.opk")
if not opkgcl.is_installed("a1"):
	cleanup()
	opk.fail("package ``a1'' not installed.")

# install ``c'' from repository
opkgcl.install("c")
if not opkgcl.is_installed("c"):
	cleanup()
	opk.fail("package ``c'' not installed.")

# This is where we currently expect failure to occur: opkg is installing 'b' and
# 'b1' when it should just install 'a' according to the issue report.
if opkgcl.is_installed("b1"):
	cleanup()
	opk.xfail("package ``b1'' is installed, but should not be.")

cleanup()
